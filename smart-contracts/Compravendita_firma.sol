/*
ATTENZIONE:
QUESTO SMART CONTRACT È FRUTTO DI UNA DRASTICA SEMPLIFICAZIONE
AL FINE DI RENDERLO COMPRENSIBILE AI NON ADDETTI AI LAVORI.
NON RAPPRESENTA UN CASO D'USO REALE.
NON ESEGUIRE IL DEPLOYMENT SULLA RETE ETHEREUM PRINCIPALE PER NESSUN MOTIVO.
LO SCOPO PERSEGUITO DA QUESTO CONTRATTO SI RAGGIUNGE
ATTRAVERSO L'IMPLEMENTAZIONE DI TOKEN ERC-721.
Per saperne di più: http://erc721.org/
*/


pragma solidity ^0.5.11; 


contract Compravendita {
    
    address payable public proprietario;
    address payable public compratore;
    string public targa = "ETH2019SI";
    uint public prezzo = 0;
    uint constant oneEther = 1 ether;
    bool public inVendita = false;
    mapping (address=>bool) public firme;
    event Acquisto(address compratore, address venditore, uint prezzo);
    
    constructor () public {
        proprietario = msg.sender;
    }
    
    function mettiInVendita(uint _prezzo, address payable _compratore) public {
        require(msg.sender == proprietario, "Solo il proprietario può mettere in vendita");
        prezzo = _prezzo * oneEther;
        inVendita = true;
        compratore = _compratore;
    }
    
    function firma() public {
        require(msg.sender == proprietario || msg.sender == compratore, "Solo proprietario e compratore possono firmare");
        firme[msg.sender] = true;
    }
    
    function togliDallaVendita(uint256) public {
        require(msg.sender == proprietario, "Solo il proprietario può togliere dalla vendita");
        require(!(firme[proprietario] && firme[compratore]), "Il contratto è firmato, non puoi togliere dalla vendita");
        firme[msg.sender] = false;
        inVendita = false;
    }
    
    function acquista() public payable {
        require(inVendita == true, "Non in vendita");
        require(msg.sender != proprietario, "Solo chi non è proprietario può acquistare");
        require(firme[proprietario], "Il proprietario non ha firmato il contratto");
        require(firme[msg.sender], "Il compratore non ha firmato il contratto");
        require(msg.value == prezzo, "Prezzo errato");
        firme[proprietario] = false;
        firme[msg.sender] = false;
        address payable venditore = proprietario;
        proprietario = compratore;
        compratore = address(0);
        venditore.transfer(msg.value);
        inVendita = false;
        prezzo = 0;
        emit Acquisto(msg.sender, venditore, msg.value);
    }
    
}
