# Blockchain e Smart Contract per giuristi 3.0

Lo scopo dell'incontro è fornire una panoramica globale su *Blockchain* e *Smart contract*, rivolgendosi specificamente ai giuristi che operano a vari livelli fra diritto e tecnologia.

In particolare, l'obiettivo è quello di fornire uno strumentario di base per comprendere **realmente**
ciò di cui media e addetti ai lavori del diritto e dell'informatica parlano senza sosta.

L'incontro avrà un carattere progressivo per rendere comprensibili a tutti i concetti che stanno alla base della frenesia tecnologica del momento.

## Interattiva

* [Presentazione Smart Contract Live](https://www.icloud.com/keynote-live/sc:06n-6LivsLPWUJOKE2Ju587nkw40pfdW2u3bdp2Scyx982LIS8jA0PFJyRSl7XWqGCT)
* [Chat del seminario](https://tlk.io/scs-bologna)

## Risorse

* [Presentazioni](presentazioni/)
* [Blockchain Demo](https://anders.com/blockchain/blockchain)
* [MetaMask](https://metamask.io)
* [Etherscan](https://etherscan.io)
* [Smart Contracts](smart-contracts/)
* [Sorgente dApp PRA](DApp-PRA/)
* [Web dApp PRA](https://raptored01.github.io) (**richiede l'installazione di [MetaMask](https://metamask.io)**)
* [Smart Contract PRA su Etherscan](https://ropsten.etherscan.io/address/0xd9c453dc11773866e4f89b65a34164acfb4c2dab)

## Video-approfondimenti

* [How secure is 256 bit security?](https://youtu.be/S9JGmA5_unY)
* [Public Key Cryptography](https://youtu.be/GSIDS_lvRv4)
* [Ever wonder how Bitcoin (and other cryptocurrencies) actually work?](https://www.youtube.com/watch?v=bBC-nXj3Ng4)
* [Vitalik Buterin - Ethereum](https://www.youtube.com/watch?v=WSN5BaCzsbo&t=5s)
* [Understanding the Ethereum Blockchain Protocol - Vitalik Buterin](https://youtu.be/gjwr-7PgpN8)

## Approfondimenti Pro

### Bitcoin & Ethereum
* [Bitcoin Whitepaper](https://bitcoin.org/bitcoin.pdf?)
* [Ethereum Whitepaper](https://github.com/ethereum/wiki/wiki/White-Paper)
* [How to create a Bitcoin wallet address from a private key](https://medium.freecodecamp.org/how-to-create-a-bitcoin-wallet-address-from-a-private-key-eca3ddd9c05f)

### More on Consensus protocol
* [Byzantine Fault Tolerance](https://medium.com/loom-network/understanding-blockchain-fundamentals-part-1-byzantine-fault-tolerance-245f46fe8419)
* [Proof of Work & Proof of Stake](https://medium.com/loom-network/understanding-blockchain-fundamentals-part-2-proof-of-work-proof-of-stake-b6ae907c7edb)
* [Delegated Proof of Stake](https://medium.com/loom-network/understanding-blockchain-fundamentals-part-3-delegated-proof-of-stake-b385a6b92ef)
* [Hasgraph Consensus](https://www.swirlds.com/downloads/SWIRLDS-TR-2016-02.pdf)

### Smart contracts
* [Nick Szabo's original 1996 smart contracts paper](http://www.fon.hum.uva.nl/rob/Courses/InformationInSpeech/CDROM/Literature/LOTwinterschool2006/szabo.best.vwh.net/smart_contracts_2.html)
* [Solidity Documentation](https://solidity.readthedocs.io/)
* [ERC20](https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md)
* [ERC721](https://github.com/ethereum/EIPs/blob/master/EIPS/eip-721.md)
* [ERC721 examples](http://erc721.org/)

## Extra

* [What is the InterPlanetary File System?](https://themerkle.com/what-is-the-interplanetary-file-system/)
* [Hyperledger](https://www.hyperledger.org/wp-content/uploads/2018/08/HL_Whitepaper_IntroductiontoHyperledger.pdf)
